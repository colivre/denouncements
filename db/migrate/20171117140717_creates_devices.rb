class CreatesDevices < ActiveRecord::Migration
  def change
    create_table :denouncements_plugin_devices do |t|
      t.timestamps
      t.integer :owner_id
      t.string  :owner_type
      t.string  :uuid,     null: false
      t.string  :tokens,   array: true, default: []
    end

    remove_column :denouncements_plugin_denouncers, :device_tokens, :string,
                                                    array: true, default: []
  end
end
