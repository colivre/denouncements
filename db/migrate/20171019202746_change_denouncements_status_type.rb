class ChangeDenouncementsStatusType < ActiveRecord::Migration
  def up
    change_column :denouncements_plugin_denouncements, :status,
                  'integer USING status::integer'
  end

  def down
    change_column :denouncements_plugin_denouncements, :status, :string
  end
end
