$(document).ready(function() {
  $('body').on('click', 'input[type=checkbox].toggle', function() {
    if ($(this).is(':checked')) {
      $(this).parent('.toggle-wrapper').addClass('checked');
    } else {
      $(this).parent('.toggle-wrapper').removeClass('checked');
    }
  })

  $('body').on('focusin', 'input[type=checkbox].toggle', function() {
    $(this).parent('.toggle-wrapper').addClass('focused');
  })

  $('body').on('focusout', 'input[type=checkbox].toggle', function() {
    $(this).parent('.toggle-wrapper').removeClass('focused');
  })
})
