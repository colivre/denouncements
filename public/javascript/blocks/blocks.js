(function() {
  $(document).ready(function() {
    $('.profile-description-block .more').on('click', function() {
      $(this).closest('.profile-description-block').find('.profile-description').toggle()
      $(this).closest('.profile-description-block').find('.profile-actions').toggle()
      return false
    })

    $('.communities-list-block .more').on('click', function() {
      $(this).closest('.communities-list-block').find('.communities-list').toggle()
      $(this).closest('.communities-list-block').find('.communities-actions').toggle()
      return false
    })

    $('.communities-list-block .join').on('click', function() {
      location.reload()
    })

    $('.communities-list-block .leave').on('click', function() {
      location.reload()
    })
  })
})()
