(function() {
  $(document).ready(function() {
    $('#identification').on('click', '#yes-btn', function() {
      $('#anonymous-denouncement input').prop('disabled', true)
      displayForm('#identified-denouncement')

      if ((typeof(google) != 'undefined') && google.maps) {
        setTimeout(function() {
          GoogleMaps.refresh()
        }, 500)
      }

      return false;
    })

    $('#identification').on('click', '#no-btn', function() {
      $('#identified-denouncement input').prop('disabled', true)
      addVictimForm();
      return displayForm('#anonymous-denouncement')
    })

    $('#identification-step #identified-denouncement')
    .on('click', '.cancel-btn', function() {
      $('#identified-denouncement').addClass('hidden')
      $('#identification-step #cancelled-identification').removeClass('hidden')
      scrollToTop()
      return false
    })

    // Validation for identified denouncement
    $('#identification-step #identified-denouncement')
    .on('click', '.continue-btn', function() {
      var name = $('#identified-denouncement input.text-person-name').val()
      var email = $('#identified-denouncement input.text-person-email').val()
      var address = $('#identified-denouncement input.text-person-address').val()

      if (name && email && address) {
        if (validEmail(email)) {
          $('#victims-step p.victim-author-msg').show()
          $('#victims-step p.not-victim-msg').hide()
          $('#identified-denouncement').addClass("hidden")
          displayForm('#socioeconomic-data')
            // StepsBar.nextStep()
          Confirmation.setIdentification(name, email, address, false)
          addAuthorVictim(name, email)
        } else {
          showError('#identified-denouncement .error-msg.invalid-email')
        }
      } else {
        showError('#identified-denouncement .error-msg.required-fields')
      }
      return false
    })

    $('#identification-step #socioeconomic-data')
    .on('click', '.continue-btn', function() {
      var birthDate          = $('#socioeconomic-data input[name="person[birth_date]"]').val()
      var ethnicity          = $('#socioeconomic-data select[name="person[ethnicity]"] option:selected').html()
      var disability         = $('#socioeconomic-data select[name="person[disability]"] option:selected').html()
      var genderIdentity     = $('#socioeconomic-data select[name="person[gender_identity]"] option:selected').html()
      var genderCondition    = $('#socioeconomic-data select[name="person[gender_condition]"] option:selected').html()
      var sexualOrientation  = $('#socioeconomic-data select[name="person[sexual_orientation]"] option:selected').html()
      Confirmation.setSocioEconomicData(birthDate, ethnicity, disability, genderIdentity, genderCondition, sexualOrientation)
      StepsBar.nextStep()
    })

    // Validation for anonymous denouncement
    $('#identification-step #anonymous-denouncement')
    .on('click', '.continue-btn', function() {
      var name = $('#anonymous-denouncement input.text-person-name').val()
      var email = $('#anonymous-denouncement input.text-person-email').val()
      var anonymous = !$('#anonymous-denouncement input.check-identify-user')
                      .is(':checked')

      if (name && email) {
        if (validEmail(email)) {
          $('#victims-step p.victim-author-msg').hide()
          $('#victims-step p.not-victim-msg').show()
          $('#victims-step #author-victim-fields input').prop('disabled', true)
          $('#victims-step #author-victim-fields input').val('')

          if ($('#victims-list .victim:not(.template)').length == 0) {
            addVictimForm()
          }

          StepsBar.nextStep()
          Confirmation.setIdentification(name, email, '-', anonymous)
        } else {
          showError('#anonymous-denouncement .error-msg.invalid-email')
        }
      } else {
        showError('#anonymous-denouncement .error-msg.required-fields')
      }
      return false
    })

    // Clear fields when pressing back or cancel
    $('#identification-step').on('click', ':not(#socioeconomic-data) .button.icon-back', function() {
      $('#identification').removeClass('hidden')
      $('#identification-step #cancelled-identification').addClass('hidden')
      $('#identification-step #socioeconomic-data').addClass('hidden')
      $('#anonymous-denouncement').addClass('hidden')
      clearForms()
      scrollToTop()
      return false
    })

    $('#identification-step #socioeconomic-data').on('click', '.button.icon-back', function() {
      $('#identification').addClass('hidden')
      $('#identification-step #socioeconomic-data').addClass('hidden')
      displayForm('#identified-denouncement')
      scrollToTop()
      return false
    })

    // Fill fields if using a Noosfero profile
    $('#identification-step').on('change', 'input.use-profile', function() {
      if ($(this).is(':checked')) {
        $('#identification-step input.text-person-name').val($(this).data('name'))
        $('#identification-step input.text-person-email').val($(this).data('email'))
        $('#identification-step input[type=text]').not('.text-person-address')
                                                  .not('#datepicker-to-date')
                                                  .addClass('disabled')
      } else {
        $('#identification-step input[type=text]').not('.text-person-address')
                                                  .val('')
        $('#identification-step input[type=text]').not('.text-person-address')
                                                  .removeClass('disabled')
      }
    })
  })

  function displayForm(el) {
    $('#identification').addClass('hidden')
    $(el).removeClass('hidden')
    scrollToTop()
    return false
  }

  function clearForms() {
    $('#identification-step input[type=text]').val('')
    $('#identification-step input[type=text]').removeClass('disabled')
    $('#identification-step input[type=checkbox]').prop('checked', false)
    $('#identification-step .toggle-wrapper').removeClass('checked')
    $('#identification-step input').prop('disabled', false)

    $('#identification-step #identify-user').prop('checked', true)
    $('#identification-step #identify-user').closest('.toggle-wrapper')
                                            .addClass('checked')

  }

  function validEmail(email) {
    var EMAIL_REGEX =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/
    return EMAIL_REGEX.test(email)
  }

})()
