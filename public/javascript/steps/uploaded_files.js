var UploadedFiles = {};

(function() {

  UploadedFiles = {

    images: new Map,
    videos: new Map,
    audios: new Map,
    undefineds: new Map,

    files: {
      IMAGE: {type: 'image', map: new Map},
      VIDEO: {type: 'video', map: new Map},
      AUDIO: {type: 'audio', map: new Map},
      UNDEFINED: {type: 'undefined', map: new Map}
    },

    getType: function(file) {
      var type = file.type.split('/')[0]
      if(getAllTypes().includes(type)) {
        return type
      } else {
        return this.files.UNDEFINED.type
      }
    },

    add: function(file) {
      var type = this.getType(file)
      var map = getMap(type)

      if(!map.has(file.name)) {
        map.set(file.name, file)
        return true
      } else {
        return false
      }
    },

    remove: function(file, type) {
      var map = getMap(type)
      map.delete(file)
    }
  }

  function getAllTypes() {
    var types = []

    $.each(UploadedFiles.files, function(key, value) {
            types.push(value.type)
    })

    return types
  }

  function getMap(type) {
    var map = UploadedFiles.files.UNDEFINED.map

    $.each(UploadedFiles.files, function(key, value) {
      if(type == value.type) {
        map = value.map
        return
      }
    })

    return map
  }

})()
