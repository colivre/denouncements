$(document).ready(function() {
  $('#occurrence-step').on('click', '.continue-btn', function() {
    var description = $('#occurrence-step textarea#denouncement-description').val()
    var address = $('#occurrence-step input#denouncement-address').val()
    var date = $('#occurrence-step input#datepicker-from-date').val()

    if(description && address && date) {
        StepsBar.nextStep()
        Confirmation.setOccurrence(date, address, description)
    } else {
        showError('#occurrence-step .error-msg')
    }
    return false
  })
})
