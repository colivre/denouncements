(function() {
  $(document).ready(function() {
    $('form#new_step_form').submit(function(event) {
      event.preventDefault()
      sendForm()
    })

    sendSavedDenouncements()
  })

  function sendForm() {
    var form = $('form#new_step_form').get(0)
    var formData = new FormData(form)

    formData.delete("denouncement_files[]")
    $.each(UploadedFiles.files, function(key, list_files) {
       list_files.map.forEach(function(file) {
          formData.append('denouncement_files[]', file)
       })
    })

    $(form).addClass('loading')

    $.ajax({
      url: form.getAttribute('action'),
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data) {
        $(form).removeClass('loading')
        if(data.location) {
          window.location.href = data.location
        } else {
          $("#denouncement-content").html(data)
        }
      },
      error: function(request, status) {
        $(form).removeClass('loading')
        saveDataOffline(formData)
      }
    })
  }

  function saveDataOffline(formData) {
    console.log("[INFO] Not possible to send data, saving it offline.")
    var store = new Store('denouncements-plugin')

    var data = []
    formData.forEach(function(value, key) {
      data.push([key, value])
    })

    store.write(data).then(function() {
      console.log("[INFO] Data saved offline.")
      window.location = '/plugin/denouncements/new'
    }).catch(function(error) {
      console.log("[ERROR] Error saving data offline: " + error)
    })
  }

  function sendSavedDenouncements() {
    var store = new Store('denouncements-plugin')
    store.listItems().then(function(entries) {
      $.each(entries, function(key, submission) {
        var formData = new FormData()
        $.each(submission, function(index, entry){
          formData.append(entry[0], entry[1])
        })

        console.log("[INFO] Trying to send denouncement again...")
        $.ajax({
          url: '/plugin/denouncements/create',
          data: formData,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data) {
            console.log("[INFO] Denouncement was saved.")
            store.remove(parseInt(key))
          },
          error: function(request, status) {
            console.log("[ERROR] Could not send. Will be retried.")
          }
        })
      })
    })
  }
})()
