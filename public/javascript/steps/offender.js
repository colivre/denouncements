$(document).ready(function() {

  $('#offender-step').on('click', '.continue-btn', function() {
    var offenders = getOffenders()
    if(offenders.length > 0) {
      StepsBar.nextStep()
      Confirmation.setOffenders(offenders)
      if (!$('#denouncement-address').val()) {
        setTimeout(function() {
          initMap('occurrence-step', false);
        }, 500)
      }
    } else {
      showErrorMessage('#offender-step .error-msg',
       '#offender-step .errors-list #offender-empty')
    }
    return false
  })

  $('#offender-step').on('click', '.button#add-offender', function() {
    if(checkLastOffender()) {
      addOffenderForm()
    } else {
      showErrorMessage('#offender-step .error-msg',
       '#offender-step .errors-list #offender-no-information')
    }
    return false;
  })

  $('.offenders-list').on('click', '.offender .remove-offender', function() {
    var offender = $(this).closest('.offender')
    removeOffender(offender)
    return false
  })

  function checkLastOffender() {
    var lastChild = $('#offenders-list .offender:not(.template):last-child')

    if(lastChild.length > 0) {
      var name = lastChild.find('input.offender-name').val()
      var relationship = lastChild.find('select.offender-relationship').val()
      return name || relationship
    } else {
      return true
    }
  }

  function getOffenders() {
    var offenders = []
    $('#offenders-list').find('.offender').not('.template').each(function(i) {
      var name = $(this).find('input.offender-name').val()
      var relationship = $(this).find('select.offender-relationship')

      if (name || relationship.val()) {
        offenders.push({name: name, relationship: getSelectedText(relationship) })
      }
    })
    return offenders
  }

  function removeOffender(offender) {

    var name = offender.find('input.offender-name').val()
    var relationship = getSelectedText(offender.find('select.offender-relationship'))
    var modal = $('div.remove-confirmation-modal.offender-modal')

    modal.find('span.remove-confirmation-name').text(name)
    modal.find('span.remove-confirmation-relationship').text(relationship)

    var shouldRemove = true

    $('.offender-modal #yes-remove-btn').on('click', function() {
      if (shouldRemove) {
        offender.closest('.offender').remove()
        $("#noosfero-modal").fadeOut(500);
      }
      return false
    })

    $('.offender-modal #no-remove-btn,' +
      '#cboxOverlay, #cboxClose').on('click', function() {
      shouldRemove = false
      $("#noosfero-modal").fadeOut(500);
      return false
    })
  }
})

function addOffenderForm() {
  var template = $('#offenders-list .offender.template ').clone()
  template.removeClass('template')

  template.find('input').prop('disabled', false)
  template.find('select').prop('disabled', false)
  template.appendTo('#offenders-list')
}
