require 'test_helper'

class StepsHelperTest < ActiveSupport::TestCase
  include DenouncementsPlugin::StepsHelper

  should 'uses a date if the string format is valid' do
    self.expects(:show_date).with('2018-10-10'.to_date)
    safe_show_date('2018-10-10')
  end

  should 'use nil if string format is not valid ' do
    self.expects(:show_date).with(nil)
    safe_show_date('not-a-date')
  end

  should 'return only the selected disabilities' do
    disabilities = { 'physical-disability' => '1',
                     'hearing-disability' => '0',
                     'visual-disability' => '0',
                     'intellectual-disability' => '1' }
    str = disabilities_to_s(disabilities)
    assert_match /#{disabilities_list['physical-disability']}/, str
    assert_no_match /#{disabilities_list['hearing-disability']}/, str
    assert_no_match /#{disabilities_list['visual-impairment']}/, str
    assert_match /#{disabilities_list['intellectual-disability']}/, str
  end
end
