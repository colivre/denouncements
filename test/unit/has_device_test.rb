require 'test_helper'

class HasDeviceTest < ActiveSupport::TestCase

  def setup
    @denouncer = DenouncementsPlugin::Denouncer.create!(name: 'ze',
                                                        email: 'ze@mail.com')
  end

  should 'create a new device if there is no one with such uuid' do
    assert_difference '@denouncer.devices.count' do
      @denouncer.update_devices('abc456', '123456')
      @denouncer.reload
    end
  end

  should 'update an existing device with the same uuid' do
    DenouncementsPlugin::Device.create!(uuid: '123fad', tokens: ['123'],
                                        owner: @denouncer)
    assert_no_difference '@denouncer.devices.count' do
      @denouncer.update_devices('123fad', '123456')
    end
  end

  should 'still create a new device even if token is blank' do
    assert_difference '@denouncer.devices.count' do
      @denouncer.update_devices('123fad', '')
    end
    assert @denouncer.device_tokens.empty?
  end

  should 'add new token to existent device' do
    device = DenouncementsPlugin::Device.create!(uuid: '123fad',
                                                 tokens: ['321456'],
                                                 owner: @denouncer)
    @denouncer.update_devices('123fad', '123456')
    device.reload
    assert_equivalent ['123456', '321456'], device.tokens
  end

  should 'returns all unique tokens' do
    DenouncementsPlugin::Device.create!(uuid: '123fad',
                                        tokens: ['123', '321'],
                                        owner: @denouncer)
    DenouncementsPlugin::Device.create!(uuid: '123fad',
                                        tokens: ['123', '456'],
                                        owner: @denouncer)
    assert_equivalent ['123', '321', '456'], @denouncer.device_tokens
  end

  should 'return tokens of all profiles' do
    DenouncementsPlugin::Device.create!(uuid: '123fad',
                                        tokens: ['123', '321'],
                                        owner: fast_create(Person))
    DenouncementsPlugin::Device.create!(uuid: '123fad',
                                        tokens: ['456'],
                                        owner: fast_create(Person))
    assert_equivalent ['123', '321', '456'], Person.all.device_tokens
  end

end
