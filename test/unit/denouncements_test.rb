require 'test_helper'

class DenouncementsTest < ActiveSupport::TestCase

  should 'create a token before the validation' do
    den = DenouncementsPlugin::Denouncement.new
    den.valid?
    assert den.present?
  end

  should 'require offenders and victims' do
    den = DenouncementsPlugin::Denouncement.new
    den.metadata = { offenders: [], victims: [] }
    den.valid?
    assert den.errors[:offenders].present?
    assert den.errors[:victims].present?
  end

  should 'not accept incomplete offenders' do
    den = DenouncementsPlugin::Denouncement.new
    den.metadata = { offenders: [
      { 'name' => 'ze', 'institution' => '', 'relationship' => 'friend' },
      { 'name' => 'rosa', 'institution' => 'foo', 'relationship' => 'boss' },
      { 'name' => '', 'institution' => '' }
    ]}
    den.valid?
    assert den.errors[:offenders].present?
  end

  should 'accept complete offenders' do
    den = DenouncementsPlugin::Denouncement.new
    den.metadata = {  offenders: [
      { 'name' => 'ze', 'institution' => 'foo', 'relationship' => 'friend' },
      { 'name' => 'rosa', 'institution' => 'foo', 'relationship' => 'boss' },
      { 'name' => 'maria', 'institution' => 'foo', 'relationship' => '' }
    ]}
    den.valid?
    refute den.errors[:offenders].present?
  end

  should 'not accept incomplete victims' do
    den = DenouncementsPlugin::Denouncement.new
    den.metadata = { victims: [
      { 'name' => 'ze', 'contact' => 'ze@mail.com' },
      { 'name' => '', 'contact' => '' },
      { 'contact' => '555 444 3299' }
    ]}
    den.valid?
    assert den.errors[:victims].present?
  end

  should 'accept complete victims' do
    den = DenouncementsPlugin::Denouncement.new
    den.metadata = { victims: [
      { 'name' => 'ze', 'contact' => 'ze@mail.com' },
      { 'name' => 'rosa', 'contact' => 'rosa@mail.com' },
      { 'name' => 'maria', 'contact' => '' }
    ]}
    den.valid?
    refute den.errors[:victims].present?
  end

  should 'return person data from denouncement' do
    den = DenouncementsPlugin::Denouncement.new
    person_data = { 'name' => 'a name', 'address' => 'some address' }
    den.metadata = { person: person_data }
    assert_equal person_data, den.person_data
  end
end
