require 'test_helper'
require 'webmock'
include WebMock::API

class DenouncementsPluginControllerTest < ActionController::TestCase

  include DenouncementsPlugin::CookiesHelper

  should 'list all denouncements to admins' do
    person1 = create_user.person
    person2 = fast_create(Person)

    Environment.default.add_admin person1

    login_as(person1.identifier)

    denouncer1 = DenouncementsPlugin::Denouncer.create!(name: 'ze',
                                                        email: 'ze@mail.com',
                                                        profile_id: person1.id)
    denouncer2 = DenouncementsPlugin::Denouncer.create!(name: 'ze',
                                                        email: 'ze@mail.com',
                                                        profile_id: person2.id)

    denouncement1 = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement1.denouncer = denouncer1
    denouncement1.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }

    denouncement1.save!
    denouncement2 = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement2.denouncer = denouncer2
    denouncement2.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    denouncement2.save!

    get :index
    assert_includes assigns(:denouncements), denouncement1
    assert_includes assigns(:denouncements), denouncement2
  end

  should 'list own denouncements to regular users' do
    person1 = create_user.person
    person2 = fast_create(Person)

    login_as(person1.identifier)

    denouncer1 = DenouncementsPlugin::Denouncer.create!(name: 'ze',
                                                        email: 'ze@mail.com',
                                                        profile_id: person1.id)
    denouncer2 = DenouncementsPlugin::Denouncer.create!(name: 'ze',
                                                        email: 'ze@mail.com',
                                                        profile_id: person2.id)

    denouncement1 = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement1.denouncer = denouncer1
    denouncement1.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    denouncement1.save!
    denouncement2 = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement2.denouncer = denouncer2
    denouncement2.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    denouncement2.save!

    get :index
    assert_includes assigns(:denouncements), denouncement1
    assert_not_includes assigns(:denouncements), denouncement2
  end

  should 'fail silently if denouncer data is invalid' do
    post :create, params: { person: nil }
    assert response.body.include? 'location'
    assert session[:notice].present?
  end

  should 'create denouncer if denouncer data is valid' do
    assert_difference 'DenouncementsPlugin::Denouncer.count' do
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
    end
  end

  should 'not create two denouncers' do
    assert_difference 'DenouncementsPlugin::Denouncer.count', 1 do
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
      post :create, person: { name: 'ze', email: 'ze@mail.com' }
    end
  end

  should 'link denouncer to profile if user is authenticated' do
    profile = create_user.person
    login_as(profile.identifier)

    post :create, person: {
      name: 'ze',
      email: 'ze@mail.com',
      id: profile.id,
      use_profile: '1'
    }
    assert_equal profile.id, DenouncementsPlugin::Denouncer.last.profile.id
  end

  should 'not link denouncer to profile if use_profile is not checked' do
    profile = fast_create(Profile)
    post :create, person: {
      name: 'ze',
      email: 'ze@mail.com',
      id: profile.id,
      use_profile: '0'
    }
    assert DenouncementsPlugin::Denouncer.last.profile.nil?
  end

  should 'create denouncement if data is valid' do
    assert_difference 'DenouncementsPlugin::Denouncement.count' do
      post :create, {
        person: { name: 'ze', email: 'ze@mail.com' },
        dncmt: denouncement_data,
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }],
        denouncement_files: [fixture_file_upload('/files/rails.png', 'image/png')]
      }
    end
  end

  should 'add push token to denouner if it exists in session' do
    cookies[device_id_cookie_key] = '123456'
    cookies[push_token_cookie_key] = 'mytoken'
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    denouncer = DenouncementsPlugin::Denouncer.last
    assert_includes denouncer.device_tokens, 'mytoken'
  end

  should 'remove blank entries from victims and offenders' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: denouncement_data,
      victims: [{}, { name: '' }, { name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{}, { name: 'rose', institution: 'foo', relationship: '?' }]
    }
    data = DenouncementsPlugin::Denouncement.last.metadata
    assert_equal 1, data['victims'].count
    assert_equal 1, data['offenders'].count
  end

  should 'save person data if user is to be identified' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com', identified: 1 },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    assert DenouncementsPlugin::Denouncement.last.metadata['person'].present?
  end

  should 'not save person data if user wants to be anonymous' do
    post :create, {
      person: { name: 'ze', email: 'ze@mail.com', identified: 0 },
      dncmt: denouncement_data,
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }
    refute DenouncementsPlugin::Denouncement.last.metadata['person'].present?
  end

  should 'Generate denouncement token after save' do
    post :create,
         person: { name: 'ze', email: 'ze@mail.com' },
         dncmt: denouncement_data,
         victims: [{ name: 'maria', contact: 'maria@mail.com' }],
         offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    assert DenouncementsPlugin::Denouncement.last.token
  end

  should 'Send a post request to external denouncement service' do
    WebMock.enable!
    stub_request(:post, DenouncementsPlugin.service_address)
      .to_return(body: 'Denouncement Registered')
    denouncer = DenouncementsPlugin::Denouncer.create(name: 'ze',
                                                      email: 'ze@mail.com')

    denouncement = DenouncementsPlugin::Denouncement.new(denouncement_data)
    denouncement.denouncer = denouncer
    denouncement.metadata =
      {
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }
    denouncement.save
    denouncement.reload

    data = {
      'denouncement' => denouncement.attributes.keep_if { |attr| attr != 'id' }
    }
    job = DenouncementsPlugin::SendDenouncementJob.new(data)
    assert_equal job.perform.body, 'Denouncement Registered'
  end

  private

  def denouncement_data
    {
      date: DateTime.now,
      address: 'some address',
      description: 'a violation'
    }
  end
end
