#!/usr/bin/env ruby

if ARGV.size != 1
  puts 'ERROR: invalid arguments'
  puts "Usage: #{$0} [environment_id]"
  exit 1
end

require_relative '../../../config/environment'

env = Environment.find_by(id: ARGV[0])
unless env
  puts "ERROR: Environment with id=#{ARGV[0]} was not found."
  exit 1
end

def create_block(klass, opts={}, data={})
  block = klass.new opts
  block.metadata = data
  block
end

def plugin_opts(env)
  { denouncements_plugin: { for_env: env.id } }
end

def create_home_page(env)
  puts 'Updating home page...'

  # TODO: fix links
  links = [
    { name: 'Proteja', address: '#', :title => 'Proteja' },
    { name: 'Participe', address: '#', :title => 'Participe' },
    { name: 'Acontece', address: '/acontece', :title => 'Acontece' }
  ]
  box = env.boxes.find_by(position: 1) || env.boxes.new
  box.position = 1
  box.save!

  box.blocks.destroy_all
  box.blocks << create_block(LinkListBlock, { links: links, css: 'home-block',
                             display: "home_page_only" }, plugin_opts(env))
  box.blocks << create_block(HighlightsBlock, { display: "home_page_only" },
                             plugin_opts(env))
  box.blocks << create_block(MainBlock, { display: "except_home_page" },
                             plugin_opts(env))
end

def create_participe_page(env)
  puts 'Creating "Participe" pages...'

  person = env.profiles.find_by(identifier: 'noosfero_person_template')
  box = person.boxes.find_by(position: 1)
  box.blocks.destroy_all
  box.blocks << create_block(ProfileDescriptionBlock, {}, plugin_opts(env))
  box.blocks << create_block(MainBlock, {}, plugin_opts(env))
  box.blocks << create_block(CommunitiesListBlock, {}, plugin_opts(env))
  box.blocks << create_block(MainBlock)
  box.save!
  person.forums.destroy_all
  person.forums.create!(name: 'Fórum')

  community = env.profiles.find_by(identifier: 'noosfero_community_template')
  box = community.boxes.find_by(position: 1)
  box.blocks.destroy_all
  box.blocks << create_block(CommunitiesListBlock, {}, plugin_opts(env))
  box.blocks << create_block(MainBlock, {}, plugin_opts(env))
  box.blocks << create_block(ProfileDescriptionBlock, {}, plugin_opts(env))
  box.blocks << create_block(MainBlock)
  community.forums.destroy_all
  community.forums.create!(name: 'Fórum')

  print 'Applying person templates'
  env.people.no_templates.each do |p|
    p.apply_template(person)
    print '.'
  end

  puts ''
  print 'Applying community templates'
  env.communities.no_templates.each do |c|
    c.apply_template(community)
    print '.'
  end
  puts ''
end

def create_acontece_page(env)
  puts 'Creating "Acontece" page...'

  acontece = env.communities.find_or_create_by(identifier: 'acontece',
                                               name: 'Acontece')
  acontece.home_page_id = acontece.blog.id
  acontece.layout_template = 'nosidebars'
  box = acontece.boxes.find_by(position: 1)
  box.blocks.destroy_all
  box.blocks << create_block(HighlightsBlock, { css: 'happens-highlights' },
                             plugin_opts(env))
  box.blocks << create_block(HappensBlock, {}, plugin_opts(env))
  box.blocks << create_block(MainBlock)
  box.blocks << create_block(MainBlock, {}, plugin_opts(env))
  acontece.save!
end

ActiveRecord::Base.transaction do
  puts 'Updating environment theme and template...'
  env.layout_template = 'nosidebars'
  env.theme = "participa-mais"
  env.metadata[:denouncements_plugin] = { layout: 'nosidebars' }

  create_home_page(env)
  create_participe_page(env)
  create_acontece_page(env)

  env.save!
end

puts 'The environment was customized.'
