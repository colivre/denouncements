class PushTokenController < PublicController

  include DenouncementsPlugin::CookiesHelper

  before_filter :accept_only_post
  skip_before_filter  :verify_authenticity_token

  def set
    return head :bad_request if params[:uuid].nil?

    set_plugin_cookie(push_token_cookie_key, params[:token])
    set_plugin_cookie(device_id_cookie_key, params[:uuid])

    DenouncementsPlugin::Device.update_tokens(params[:uuid], params[:token])
    if current_person
      # Also links device to profile if user is logged in
      current_person.update_devices(params[:uuid], params[:token])
    end
    head :ok
  end

end
