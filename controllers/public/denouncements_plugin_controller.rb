class DenouncementsPluginController < PublicController

  include DenouncementsPlugin::CookiesHelper
  helper DenouncementsPlugin::StepsHelper

  no_design_blocks

  def index
    if current_user.present?
      @denouncements = if current_person.is_admin?
                         DenouncementsPlugin::Denouncement.all
                       else
                         current_person.denouncements
                       end
      @denouncements = @denouncements.paginate(per_page: per_page,
                                               page: params[:npage])
    else
      render_access_denied
    end
  end

  def menu
    unless current_user.present?
      redirect_to action: :new
    end
  end

  def new
    @denouncement = DenouncementsPlugin::Denouncement.new
  end

  def create
    denouncer = DenouncementsPlugin::Denouncer.find_or_initialize_by denouncer_data
    @denouncement = DenouncementsPlugin::Denouncement.new(params['dncmt'])
    @denouncement.denouncer = denouncer
    @denouncement.metadata = denouncement_data
    @denouncement.media_files = upload_files

    unless denouncer.save && @denouncement.save
      session[:notice] = _('Ops, Something went wrong.')
      render :json => {
          :location => url_for(:controller => 'denouncements_plugin', :action => 'new')
      }, :status => 400
    end
    denouncer.update_devices(cookies[device_id_cookie_key],
                             cookies[push_token_cookie_key])
  end

  def show
    @denouncement = DenouncementsPlugin::Denouncement.find_by(token: params[:token])
    if @denouncement.present? && !(current_person.try(&:is_admin?) ||
                                   current_person == @denouncement.profile)
      @denouncement = nil
    end
  end

  def download_file
    denouncement = DenouncementsPlugin::Denouncement.find_by(token: params[:token])
    if denouncement.present? && (current_person.try(&:is_admin?) ||
                                 current_person == denouncement.profile)
      file = denouncement.media_files.find_by(id: params[:file_id])
      if file.present?
        send_data file.data, file.download_headers
      else
        render_not_found
      end
    else
      render_not_found
    end
  end

  private

  def validate_request_origin
    config = DenouncementsPlugin.config
    remote_service = request.headers['REMOTE_ADDR']
    if config['dncmt_service']['addr'] != remote_service
      render json: { 'error' => 'You are not allowed to perform this action' },
             status: 403
    end
  end

  def sanitize_list(list)
    return [] unless list

    valids = []
    list.each do |entry|
      valids << entry if entry.present? && entry.values.any? { |v| v.present? }
    end
    valids
  end

  def denouncer_data
    person = (params['person'] || {}).stringify_keys
    data = person.slice('name', 'email')
    data.merge({ profile_id: current_person.try(:id) })
  end

  def denouncement_data
    data = {
      offenders: sanitize_list(params['offenders']),
      victims: sanitize_list(params['victims'])
    }
    if params['person'].present? && params['person']['identified'] == '1'
      data = data.merge({
        person: params['person']
      })
    end
    data
  end

  def upload_files
    uploaded_files = []
    if params.has_key?(:denouncement_files)
      params[:denouncement_files].each do |file|
        unless file == ''
          uploaded_files << DenouncementsPlugin::MediaFile.new(
            {
              :uploaded_data => file
            },
            :whithout_protection => true
          )
        end
      end
    end
    uploaded_files
  end

  def per_page
    15
  end
end
