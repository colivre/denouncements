module DenouncementsPlugin::StepsHelper

  def safe_show_date(str_date)
    date = begin
             str_date.to_date
           rescue ArgumentError
             nil
           end
    show_date(date)
  end

  def step_status(current, index)
    if current > index
      'past'
    elsif current == index
      'past active'
    else
      'future'
    end
  end

  def toggleable_text_field(name, toggle_msg, opts={})
    classes = { class: "#{opts.delete(:class)} text-field" }
    opts = opts.merge({ data: { alt: toggle_msg } })
    field = text_field_tag(name, opts.delete(:value), opts.merge(classes))

    toggle = toggle(toggle_msg, "#{name}_toggle", 1, nil)
    content_tag(:div, field + toggle, class: 'toggleable-field')
  end

  def toggleable_select_field(name, toggle_msg, collection, opts={})
    classes = { class: "#{opts.delete(:class)} select-field" }
    opts = opts.merge({ data: { alt: toggle_msg } })
    field = select_tag(name, options_from_collection_for_select(collection, :first, :last),
                        opts.merge(classes))

    toggle = toggle(toggle_msg, "#{name}_toggle", 1, nil)
    content_tag(:div, field + toggle, class: 'toggleable-select')
  end

  def toggle(msg, name, value, checked=false, opts={})
    classes = { class: "toggle #{opts[:class]}" }
    toggle = check_box_tag(name, value, checked, opts.merge(classes))
    label_classes = "toggle-wrapper #{ checked ? 'checked' : '' }"
    content_tag(:label, toggle + msg, class:  label_classes)
  end

  def relationship_list
    {
        'mother' => _('Mother'),
        'father' => _('Father'),
        'daughter' => _('Daughter'),
        'son' => _('Son'),
        'sister' => _('Sister'),
        'brother' => _('Brother'),
        'grandmother' => _('Grandmother'),
        'grandfather' => _('Grandfather'),
        'granddaughter' => _('Granddaughter'),
        'grandson' => _('Grandson'),
        'aunt' => _('Aunt'),
        'uncle' => _('Uncle'),
        'female-cousin' => _('Female Cousin'),
        'male-cousin' => _('Male Cousin'),
        'niece' => _('Niece'),
        'nephew' => _('Nephew'),
        'wife' => _('Wife'),
        'husband' => _('Husband'),
        'female-spouse' => _('Female Spouse - stable or informal union'),
        'male-spouse' => _('Male Spouse - stable or informal union'),
        'mother-in-law' => _('Mother-in-law'),
        'father-in-law' => _('Father-in-law'),
        'daughter-in-law' => _('Daughter-in-law'),
        'son-in-law' => _('Son-in-law'),
        'sister-in-law' => _('Sister-in-law'),
        'brother-in-law' => _('Brother-in-law'),
        'stepmother' => _('Stepmother'),
        'stepfather' => _('Stepfather'),
        'stepdaughter' => _('Stepdaughter'),
        'stepson' => _('Stepson'),
        'lover' => _('Lover'),
        'female-friend' => _('Female Friend'),
        'male-friend' => _('Male Friend'),
        'female-neighbor' => _('Female Neighbor'),
        'male-neighbor' => _('Male Neighbor'),
        'girlfriend' => _('Girlfriend'),
        'boyfriend' => _('Boyfriend'),
        'female-curator' => _('Female Curator'),
        'male-curator' => _('Male Curator'),
        'female-tutor' => _('Female Tutor'),
        'male-tutor' => _('Male Tutor'),
        'coworker' => _('Coworker'),
        'female-employer' => _('Female Employer'),
        'male-employer' => _('Male Employer'),
        'female-employee' => _('Female Employee'),
        'male-employee' => _('Male Employee'),
        'female-unknown' => _('Female Unknown'),
        'male-unknown' => _('Male Known'),
        'no-kinship-relation' => _('No kinship relation')
    }
  end

  def ethnicity_list
    {
      "white" => _("White"),
      "black" => _("Black"),
      "brown" => _("Brown"),
      "yellow" => _("Yellow"),
      "gypsy" => _("Gypsy"),
      "indigenous" => _("Indigenous"),
      "don't know" => _("Don't know"),
      "don't wish to report" => _("Don't wish to report")
    }
  end

  def gender_identity_list
    {
      "male" => _("Male"),
      "female" => _("Female"),
      "non-binary" => _("Non-binary"),
      "don't know" => _("Don't know"),
      "don't wish to report" => _("Don't wish to report"),
    }
  end

  def gender_condition_list
    {
      "cisgender" => _("Cisgender"),
      "transsexual" => _("Transsexual"),
      "transvestite" => _("Transvestite"),
      "don't know" => _("Don't know"),
      "don't wish to report" => _("Don't wish to report")
    }
  end

  def sexual_orientation_list
    {
      "homosexual" => _("Homosexual"),
      "heterosexual" => _("Heterosexual"),
      "assexual" => _("Assexual"),
      "bisexual" => _("Bisexual"),
      "other" => _("Other"),
      "don't know" => _("Don't know"),
      "don't wish to report" => _("Don't wish to report")
    }
  end

  def disabilities_list
    {
      'physical-disability' => _('Physical disability'),
      'hearing-disability' => _('Hearing disability'),
      'visual-impairment' => _('Visual impairment'),
      'intellectual-disability' => _('Intellectual disability')
    }
  end

  def disabilities_to_s(disabilities)
    disabilities = disabilities.select { |_, v| v == '1' }
                               .map { |k, _| disabilities_list[k] }
    disabilities.present? ? disabilities.join(', ') : _('None')
  end
end
