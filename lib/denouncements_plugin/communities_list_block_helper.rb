module DenouncementsPlugin::CommunitiesListBlockHelper

  def role_in person, community
    member_role    = Role.find_by(name: "Member")
    moderator_role = Role.find_by(name: "Moderator")
    admin_role     = Role.find_by(name: "Profile Administrator")
    if community.members_by_role(admin_role).include?(person)
      admin_role
    elsif community.members_by_role(moderator_role).include?(person)
      moderator_role
    elsif community.members_by_role(member_role).include?(person)
      member_role
    end
  end
end
