class DenouncementsPlugin::Denouncement < ApplicationRecord

  attr_accessible :token, :date, :protocol, :status, :city, :description,
                  :address

  def notification_data
    { 'protocol' => token, 'type' => 'DENOUNCEMENT_UPDATE' }
  end

  belongs_to :denouncer, class_name: 'DenouncementsPlugin::Denouncer'
  belongs_to :user
  has_many :media_files, class_name: 'DenouncementsPlugin::MediaFile'

  delegate :profile, to: :denouncer, allow_nil: true

  store_accessor :metadata
  before_save :add_token

  validates_presence_of :date, :address, :description
  validate :victims_are_valid
  validate :offenders_are_valid
  validate :denouncer_is_victim

  def tokens
    denouncer.device_tokens
  end

  def person_data
    metadata['person']
  end

  private

  def add_token
    self.token ||= SecureRandom.uuid
  end

  def victims_are_valid
    validate_records(:victims, [:name, :contact])
  end

  def offenders_are_valid
    validate_records(:offenders, [:name, :institution, :relationship])
  end

  def denouncer_is_victim
    # TODO: if denouncer is victim, check if its in metadata[:victims]
  end

  def validate_records(field, fields)
    entries = metadata[field.to_s]
    if entries.blank?
      errors.add field, _('cannot be empty')
      return
    end

    entries.each do |entry|
      if fields.all? { |f| entry[f.to_s].blank? }
        errors.add field, _('one of the entries is not valid')
        break
      end
    end
  end
end
