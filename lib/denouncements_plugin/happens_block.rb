class HappensBlock < Block

  attr_accessible :accessor_id, :accessor_type, :role_id, :resource_id, :resource_type

  def self.description
    _("Happens Block.")
  end

  def self.short_description
    _('Happens Block')
  end

  def self.pretty_name
    _('Happens Block')
  end

  def default_title
    _('Happens')
  end

  def help
    _("This block is used in happens page on participa-mais app.")
  end

end
