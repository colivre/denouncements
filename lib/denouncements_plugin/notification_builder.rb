require 'yaml'

module DenouncementsPlugin::PushNotifier

  def self.deliver(tokens, data)
    return if tokens.empty?
    Notification.new(tokens, data).notify
  end

  class Notification

    attr_accessor :device_tokens, :config, :message, :title, :data

    def initialize(device_tokens, data)
      @device_tokens = device_tokens
      @title = data[:title] || ''
      @message = data[:message] || ''
      @data = data[:data] || {}
      @config = DenouncementsPlugin.config
      create_app
    end

    def notify
      notification = Rpush::Gcm::Notification.new
      notification.app = Rpush::Gcm::App.find_by_name(config['android']['name'])
      notification.registration_ids = device_tokens
      notification.notification = { body: message,
                                    title: title,
                                    icon: 'notification_icon' }
      notification.data = data
      notification.save!
    end

    private

    def create_app
      unless Rpush::Gcm::App.find_by_name(config['android']['name'])
        app = Rpush::Gcm::App.new
        app.name = config['android']['name']
        # auth_key is a firebase api key.
        app.auth_key = config['android']['auth_key']
        app.connections = config['android']['connections']
        app.save!
      end
    end
  end
end
