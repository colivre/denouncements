class DenouncementsPlugin::MediaFile < ApplicationRecord

  include UploadSanitizer

  attr_accessible :uploaded_data, :denouncement

  belongs_to :denouncement, class_name: 'DenoucementsPlugin::Denouncement'

  def self.max_size
    5.megabytes
  end

  UPLOAD_FOLDER = 'private-files/plugins/denouncements'

  has_attachment :storage => :file_system,
    :max_size => max_size,
    :path_prefix => UPLOAD_FOLDER

  def data
    File.read(full_filename)
  end

  def download_headers
    disposition = content_type == 'application/pdf' ? 'inline' : 'attachment'
    { filename: filename, type: content_type, disposition: disposition }
  end
end
