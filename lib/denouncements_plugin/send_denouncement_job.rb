require 'net/http'
require 'uri'

class DenouncementsPlugin::SendDenouncementJob < Struct.new(:data)

  # We're using https://github.com/typicode/json-server to fake a REST API.
  # This fake API represents the external denouncement service
  # that we will use in the future.

  def perform
    uri = URI(DenouncementsPlugin.service_address)
    req = Net::HTTP::Post.new uri
    req.body = data.to_json
    req.content_type = 'application/json'
    begin
      Net::HTTP.start(uri.host, uri.port) do |http|
        return http.request(req)
      end
    rescue StandardError => err
      puts 'Failed to send denouncement to external service'
      puts err
    end
  end

  private
end
