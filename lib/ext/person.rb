require_dependency 'person'

class Person

  include DenouncementsPlugin::HasDevice

  has_one :denouncer, class_name: 'DenouncementsPlugin::Denouncer',
                      foreign_key: :profile_id
  has_many :denouncements, class_name: 'DenouncementsPlugin::Denouncement',
           through: :denouncer

end
