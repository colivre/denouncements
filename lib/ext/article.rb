require_dependency 'article'

class Article

  include Notifiable

  after_save :notify_publication, if: :just_published?

  will_notify :new_content, mailer: nil

  def tokens
   devices_to_notify = []
   if self.profile.respond_to?(:members)
     self.profile.members.each do |member|
       next if self.author == member
       devices_to_notify.concat(member.device_tokens) if self.display_to?(member)
     end
   end
   devices_to_notify
  end

  def denouncements_metadata
    self.metadata['denouncements'] ||= {}
  end

  def send_notifications?
    if self.is_a?(Folder)
      self.denouncements_metadata['should_notify'] == '1'
    elsif self.try(:parent).is_a?(Folder)
      if self.id.present?
        self.denouncements_metadata['should_notify'] == '1'
      else
        (self.denouncements_metadata['should_notify'] ||
         self.parent.denouncements_metadata['should_notify']) == '1'
      end
    end
  end

  def push_notification_message
    messages = {
      'text_article' => _("A new text article was published by %s.") % self.author.name,
      'folder' => _("A new folder was created by %s.") % self.author.name,
    }
    content_class = self.class.short_description.downcase.gsub(/\s/, '_')
    return messages[content_class] if messages.keys.include? content_class
    'No notification message available.'
  end

  private

  def notify_publication
    if !self.is_a?(Folder) && send_notifications?
      notify(:new_content, self)
    end
  end

  def just_published?
    # If published was false and set to true. It relies on the fact
    # that published may not be set back to false.
    new_record = self.id_was.nil?
    (!self.published_was || new_record) && self.published
  end

end
