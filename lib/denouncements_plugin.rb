class DenouncementsPlugin < Noosfero::Plugin

  def organization_members(organization)
    if DenouncementsPlugin.config['communities'].include?\
        (organization.identifier)
      return Person.all
    end
  end

  def html_tag_classes
    current_controller = controller_name || params[:controller]
    current_action     = action_name     || params[:action]
    -> do
      if profile && profile.person?
        'myself' if current_controller == 'profile' && current_action == 'index' && profile == current_person
      end
    end
  end

  class << self
    def plugin_name
      'DenouncementsPlugin'
    end

    def plugin_description
      _('A plugin to create and monitoring denouncements.')
    end

    def config
      begin
        conf = YAML::load(
          File.open(Rails.root + 'plugins/denouncements/config.yml')
        )
      rescue StandardError => err
        p 'Could not open config.yml file'
        p err
      end
      conf
    end

    def service_address
      ip = DenouncementsPlugin.config['dncmt_service']['addr']
      method = DenouncementsPlugin.config['dncmt_service']['method']
      port = DenouncementsPlugin.config['dncmt_service']['port']
      action = DenouncementsPlugin.config['dncmt_service']['endpoint']
      [method, ip, ':', port, '/', action].join
    end

    def extra_blocks
      { ProfileDescriptionBlock => {}, CommunitiesListBlock => {}, HappensBlock => {} }
    end

  end

  def stylesheet?
    true
  end

  def js_files
    #TODO: should we include steps Js locally w/ javascript_iclude_tag?
    [
      'map',
      'steps_bar',
      'toggleable_field',
      'toggle',
      'tokens',
      'fields',
      'steps/confirmation',
      'steps/identification',
      'steps/occurrence',
      'steps/offender',
      'steps/media_files',
      'steps/send_form',
      'steps/uploaded_files',
      'steps/victims',
      'store',
      'blocks/blocks',
      'communities'
    ].map { |filename| "javascript/#{filename}" }
  end

  def account_controller_filters
    block = proc do
      extend DenouncementsPlugin::CookiesHelper
      if (current_person &&
          cookies[push_token_cookie_key].present? &&
          cookies[device_id_cookie_key].present?)
        current_person.update_devices(cookies[device_id_cookie_key],
                                      cookies[push_token_cookie_key])
      end
    end

    {
      type: 'after_filter',
      method_name: 'denouncements_plugin_updates_profile_push_token',
      options: { only: 'login' },
      block: block
    }
  end

  def cms_extra_options
    Proc.new { render partial: 'denouncements_plugin_cms/extra_options' }
  end

  def person_friends(person)
    person.environment.people
  end

  def custom_notification(verb, *args)
    if self.respond_to? "#{verb}_notification"
      self.send("#{verb}_notification", *args)
    end
  end

  def cache_urls
    assets = js_files.map{ |f| "/assets/plugins/denouncements/#{f}.js" }
    [
      '/plugin/denouncements/new',
      '/assets/plugins/denouncements/style.css'
    ] + assets
  end

  private

  def new_content_notification(content)
    DenouncementsPlugin::PushNotifier.deliver(content.tokens, {
      title: content.name,
      message: content.push_notification_message,
      data: {
        type: 'NEW_CONTENT',
        path: content.full_path
      }
    })
  end
end
